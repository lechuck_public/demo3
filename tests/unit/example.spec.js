import { shallowMount } from '@vue/test-utils';
import Calculator from '@/components/Calculator.vue';

describe('Calculator.vue', () => {
  it('Calculates 1 + 1', () => {
    const wrapper = shallowMount(Calculator, {});
    wrapper.find('input[id="left"]').setValue('1');
    wrapper.find('input[id="right"]').setValue('1');
    expect(wrapper.find('span[id="result"]').text()).toBe('2');
  });

  it('Calculates -3 + 1', () => {
    const wrapper = shallowMount(Calculator, {});
    wrapper.find('input[id="left"]').setValue('-3');
    wrapper.find('input[id="right"]').setValue('1');
    expect(wrapper.find('span[id="result"]').text()).toBe('-2');
  });

  it('Calculates 0.6 + 0.5', () => {
    const wrapper = shallowMount(Calculator, {});
    wrapper.find('input[id="left"]').setValue('0.6');
    wrapper.find('input[id="right"]').setValue('0.5');
    expect(wrapper.find('span[id="result"]').text()).toBe('1.1');
  });

  it('Calculates a + 1', () => {
    const wrapper = shallowMount(Calculator, {});
    wrapper.find('input[id="left"]').setValue('a');
    wrapper.find('input[id="right"]').setValue('1');
    expect(wrapper.find('span[id="result"]').text()).toBe('a1');
  });
});
