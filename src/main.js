import Vue from 'vue';
import App from './App.vue';
import * as Sentry from '@sentry/browser';

Sentry.init({
  dsn: 'https://46f1e6d4145946e6896cd5ece0fbb65d@sentry.io/1338790',
  environment: process.env.VUE_APP_SENTRY_ENV,
  integrations: [new Sentry.Integrations.Vue({ Vue })]
})

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
